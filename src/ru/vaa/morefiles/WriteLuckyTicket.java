package ru.vaa.morefiles;

import java.io.*;

public class WriteLuckyTicket {
    public static void main(String[] args) {
        try (DataInputStream inputStream = new DataInputStream(new FileInputStream("C:\\Work\\moreFiles\\src\\ru\\vaa\\morefiles\\intdata.dat")); DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("C:\\Work\\moreFiles\\src\\ru\\vaa\\morefiles\\lucky.txt"))) {
            int num;
            while (inputStream.available() > 0) {
                num = inputStream.readInt();
                if (isLucky(num)) {
                    outputStream.writeBytes(String.valueOf(num) + "\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean isLucky(int num) {
        int i1, i2, i3, i4, i5, i6;
        i1 = num / 100000 % 10;
        i2 = num / 10000 % 10;
        i3 = num / 1000 % 10;
        i4 = num / 100 % 10;
        i5 = num / 10 % 10;
        i6 = num % 10;
        if ((i1 + i2 + i3) == (i4 + i5 + i6)) {
            return true;
        } else {
            return false;
        }
    }
}
