package ru.vaa.morefiles;

import java.io.*;

public class ReadWrite {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Work\\moreFiles\\src\\ru\\vaa\\morefiles\\intdata.txt")); DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("C:\\Work\\moreFiles\\src\\ru\\vaa\\morefiles\\intdata.dat"))) {
            String str;
            while ((str = bufferedReader.readLine()) != null) {
                if (str.length() == 6) {
                    int num;
                    num = Integer.parseInt(str);
                    outputStream.writeInt(num);
                }
            }
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
